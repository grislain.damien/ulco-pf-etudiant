myLast :: [a] -> a
myLast [x] = x
myLast (_:xs) = myLast xs


myInit :: [a] -> [a]
myInit [x] = []
myInit (x:xs) = x:myInit xs 


myreplicate :: Int -> a -> [a]
myreplicate 0 _ = []
myreplicate n b = b:myreplicate (n-1) b


mydrop :: Int -> [a] -> [a]
mydrop 0 list = list
mydrop n (_:xs) = mydrop (n-1) xs


mytake :: Int -> [a] -> [a]
mytake 0 list = list
mytake n list = mytake (n-1) $ myInit list
