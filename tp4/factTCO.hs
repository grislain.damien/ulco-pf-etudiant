fact :: Integer -> Integer
fact 0 = 1
fact n = n * fact (n-1)


factTco :: Integer -> Integer
factTco x = aux x 1
    where
        aux 0 acc = acc
        aux n acc = aux (n-1) (n*acc)

main :: IO ()
main = do
    print $ factTco 10
