loopEcho :: Int -> IO String
loopEcho 0 = return "loop terminated"
loopEcho n = do
    putStr "> "
    a <- getLine :: IO String
    case null a of
        True -> return "empty line"
        False -> do
            putStrLn a
            loopEcho (n-1)


main :: IO ()
main = do
    r <- loopEcho 3
    putStrLn r
            