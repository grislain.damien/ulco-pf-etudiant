pgcd :: Int -> Int -> Int
pgcd a 0 = a
pgcd a b = pgcd b (a `mod` b)
