import Data.Char

mylength :: [a] -> Int
mylength [] = 0
mylength (_:xs)  = 1 + mylength xs


toUpperString :: String -> String
toUpperString [] = ""
toUpperString (x:xs) =  (toUpper x):toUpperString xs

                
onlyLetters :: String -> String
onlyLetters [] = ""
onlyLetters (x:xs) = case isLetter x of
                        False -> onlyLetters xs
                        _ -> x:onlyLetters xs