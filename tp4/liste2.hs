mulListe :: (Num a) => a -> [a] -> [a]
mulListe _ [] = []
mulListe n (x:xs) = (n * x):mulListe n xs


selectListe :: (Ord a) => (a, a) -> [a] -> [a]
selectListe _ [] = []
selectListe (a, b) (x:xs) = if (x >= a) && (x <= b) then x:selectListe (a, b) xs else selectListe (a, b) xs


sumListe :: (Num a) => [a] -> a
sumListe [] = 0
sumListe (x:xs) = x + sumListe xs


main :: IO ()
main = do
    putStrLn "main vide"