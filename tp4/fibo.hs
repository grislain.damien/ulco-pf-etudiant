import System.Environment

fiboNaive :: Int -> Int
fiboNaive 0 = 0
fiboNaive 1 = 1
fiboNaive n = fiboNaive (n-1) + fiboNaive (n-2)

fiboTco :: Int -> Int
fiboTco x = local x 0 1
        where local 0 a _ = a
              local 1 _ b = b
              local n a b = local (n-1) b (a+b)

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["naive", nStr] -> print (fiboNaive (read nStr))
        ["tco", nStr] -> print (fiboTco (read nStr))
        _ -> putStrLn "usage: <naive|tco> <n>"
            