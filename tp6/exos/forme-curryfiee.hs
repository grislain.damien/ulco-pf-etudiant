myRangeTuple1 :: (Int, Int) -> [Int]
myRangeTuple1 (x0, x1) = [x0..x1]

myRangeCurry1 :: Int -> Int -> [Int]
myRangeCurry1 a b = [a..b]

myRangeTuple2 :: Int -> [Int]
myRangeTuple2 x1 = myRangeTuple1 (0, x1)

myRangeCurry2 :: Int -> [Int]
myRangeCurry2 x = myRangeCurry1 0 x

myCurry :: ((Int, Int) -> [Int]) -> Int -> Int -> [Int]
myCurry f1 x0 x1 = f1 (x0, x1)
