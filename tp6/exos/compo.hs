add42 :: Int -> Int
add42 x = x + 42

estPositif :: Int -> Bool
estPositif x = x > 0

plus42Positif :: Int -> Bool
plus42Positif = estPositif . add42

mul2 :: (Num a) => a -> a
mul2 x = 2*x

mul2PlusUn :: Integer -> Integer
mul2PlusUn = (+1) . (*2)

mul2MoinsUn :: Integer -> Integer
mul2MoinsUn = (+(-1)) . (*2)

applyTwice :: (Integer -> Integer) -> Integer -> Integer
applyTwice f = f . f 
