myTake2  :: [Int] -> [Int] 
myTake2 = take 2

myGet2  :: [Int] -> Int 
myGet2 = (!! 2)

-- map (*2) [1..4]
-- map (`div` 2) [1..4]