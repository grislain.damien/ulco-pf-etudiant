doubler :: Num a => [a] -> [a]
doubler list = [2*x | x<-list]

pairs :: Integral a => [a] -> [a]
pairs list = [x | x<-list, even x]

mymap :: (a -> b) -> [a] -> [b]
mymap f list = [f x | x <- list]

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter p list = [x | x<-list, p x]

multiples :: Int -> [Int]
multiples n = [x | x<-[1..n], n `mod` x == 0]

combinaisons :: [a] -> [b] -> [(a, b)]
combinaisons l1 l2 = [(x, y) | x<-l1, y<-l2]

tripletsPyth :: Int -> [(Int, Int, Int)]
tripletsPyth n = [(x, y, z) | x<-[1..n], y<-[x..n], z<-[y..n], (x*x)+(y*y)==(z*z)]