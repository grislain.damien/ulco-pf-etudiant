foldSum1 :: (Num a) => [a] -> a
foldSum1 [] = 0
foldSum1 (x:xs) = x + (foldSum1 xs)

foldSum2 :: (Num a) => [a] -> a
foldSum2 list = foldl (+) 0 list

myfoldl :: (b -> a -> b) -> b -> [a] -> b
myfoldl _ acc [] = acc
myfoldl f acc (x:xs) = myfoldl f (f acc x) xs
