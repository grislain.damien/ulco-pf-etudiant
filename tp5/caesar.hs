import Data.Char
import Data.List

decaler :: Int -> Char -> Char
decaler n _char | x >= 97 && x <= 122 = chr x
            where x = (ord _char) + n

--compterOccurences :: Char -> String -> Int
-- ...
--compterLettres :: String -> Int
-- ...
--frequencesUk :: [Float]
-- frequencesUk = 
--     [ 0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07
--   , 0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06
--    , 0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001
--    ]

-- calculerFrequences :: String -> [Float]
-- ...
-- calculerKhi2 :: [Float] -> [Float] -> Float
-- ...
-- casserCesar :: String -> [Float] -> Int
-- ...