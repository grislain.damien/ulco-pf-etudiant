mapDoubler1 :: (Num a) => [a] -> [a]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = (x*2) : (mapDoubler1 xs)


mapDoubler2 :: (Num a) => [a] -> [a]
mapDoubler2 [] = []
mapDoubler2 list = map (*2) list

mymap :: (a -> b) -> [a] -> [b]
mymap _ [] = []
mymap f (x:xs) = (f x) : (mymap f xs)
