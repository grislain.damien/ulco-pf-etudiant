import System.Random

getWords :: [String]
getWords = [
    "function",
    "functionnal programming",
    "haskell",
    "list",
    "pattern matching", 
    "recursion", 
    "tuple", 
    "type system"]

getState :: [String]
getState = [
    "                ",
    "________________",
    "    |             \n\
    \    |             \n\
    \    |             \n\
    \    |             \n\
    \    |             \n\
    \____|___________",
    "     _______      \n\
    \    |/            \n\
    \    |             \n\
    \    |             \n\
    \    |             \n\
    \    |             \n\
    \____|___________",
    "     _______      \n\
    \    |/      |     \n\
    \    |             \n\
    \    |             \n\
    \    |             \n\
    \    |             \n\
    \____|___________ ",
    "     _______      \n\
    \    |/      |     \n\
    \    |      (_)    \n\
    \    |             \n\
    \    |             \n\
    \    |             \n\
    \____|___________ ",
    "     _______      \n\
    \    |/      |     \n\
    \    |      (_)    \n\
    \    |       |     \n\
    \    |       |     \n\
    \    |             \n\
    \____|___________ ",
    "     _______      \n\
    \    |/      |     \n\
    \    |      (_)    \n\
    \    |      \\|/   \n\
    \    |       |     \n\
    \    |             \n\
    \____|___________ ",
    "     _______      \n\
    \    |/      |     \n\
    \    |      (_)    \n\
    \    |      \\|/   \n\
    \    |       |     \n\
    \    |      / \\   \n\
    \____|___________ "]

hiddenChar :: String -> String -> String
hiddenChar xs ys = [if elem x ys then x else '-' | x <- xs] 

play :: String -> [String] -> [Char] -> IO()
play word state current = do
    let wordCurrent = (hiddenChar word current)
    putStrLn $ head state
    putStrLn $ wordCurrent ++ " -> " ++ reverse current
    if wordCurrent == word then
        putStrLn "You win !"
    else if head state == (getState !! 8) then
        putStrLn $ word ++ "\nGame over !"
    else do 
        input <- getLine
        if (length input) > 0 then do
            let save = current ++ [head input]
            if (any . (==)) (head input) word then
                play word state save
            else
                play word (tail state) save
        else play word state current


main :: IO ()
main = do
    randomPick <- randomRIO (0,7 :: Int)
    play (getWords !! randomPick) getState ""