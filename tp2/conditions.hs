formaterParite :: Int -> String
formaterParite n = 
    if even n -- even pair / odd impair
        then "pair"
        else "impair"

formaterPariteGarde:: Int -> String
formaterPariteGarde n 
    | even n = "pair"
    | otherwise = "impair"

formaterSigne :: Int -> String
formaterSigne n = 
    if n == 0 
    then "nul"
    else if n > 0 
        then "positif"
        else "negatif"
        
formaterSigneGarde :: Int -> String
formaterSigneGarde n 
    | n == 0 = "nul"
    | n <= 0 = "negatif"
    | otherwise = "positif"

main :: IO()
main = do
    putStrLn (formaterParite 2)
    putStrLn (formaterParite 3)
    putStrLn (formaterSigne 2)
    putStrLn (formaterSigne (-2))
