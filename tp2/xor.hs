xor1:: Bool -> Bool -> Bool
xor1 a b = (a && not b) || (not a && b)

xor2:: Bool -> Bool -> Bool
xor2 a b = if a then not b else b

xor3:: Bool -> Bool -> Bool
xor3 a b = case (a,b) of
            (True,True)->False
            (False,False)->False
            _ -> True

xor4:: Bool -> Bool -> Bool
xor4 a b = 
    | a == b = False
    | otherwise = True

xor5:: Bool -> Bool -> Bool
xor5 True True = False
xor5 False False = False
xo5  _ _ = True


main :: IO ()
main = do
    --xor1
    putStrLn "\nXor1"
    print (True `Xor1` True)
    print (True `Xor1` False)
    print (False `Xor1` True)
    print (False `Xor1` False)
    --xor2
    putStrLn "\nXor2"
    print (True `Xor2` True)
    print (True `Xor2` False)
    print (False `Xor2` True)
    print (False `Xor2` False)
    --xor3
    putStrLn "\nXor3"
    print (True `Xor3` True)
    print (True `Xor3` False)
    print (False `Xor3` True)
    print (False `Xor3` False)
    --xor4
    putStrLn "\nXor4"
    print (True `Xor4` True)
    print (True `Xor4` False)
    print (False `Xor4` True)
    print (False `Xor4` False)
    --xor5
    putStrLn "\nXor5"
    print (True `Xor5` True)
    print (True `Xor5` False)
    print (False `Xor5` True)
    print (False `Xor5` False)
