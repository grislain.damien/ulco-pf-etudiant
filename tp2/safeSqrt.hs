safeSqrt :: Double -> Maybe Double
safeSqrt d
  | d < 0 = Nothing
  | otherwise = Just (sqrt d)

formatedSqrt :: Double -> String
formatedSqrt x = "sqrt("++ show x ++")" ++res
    where res = case safeSqrt x of
      Nothing -> "sqrt(" ++ show x ++ ") is not defined"
      Just r -> "sqrt(" ++ show x ++ ") = " ++ show r