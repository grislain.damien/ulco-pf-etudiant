fibo :: Int -> Int -- rappel : fibo(0)=0, fibo(1)=1 et fibo(n)=fibo(n-1)+fibo(n-2)
fibo n = case n of
    0 -> 0
    1 -> 1
    n -> fibo(n-1) + fibo(n-2)

fibo::Int -> Int
fibo 0 = 0
fibo 1 = 1
fibo n = fibo(n-1) + fibo(n-2)

main :: IO ()
main = do
    print (fibo 9)