borneEtFormate1 :: Double -> String
borneEtFormate1 x = show x ++ " -> " ++ show x'
    where x' = if x < 0
                then 0
                else if x > 1
                     then 1
                     else x

borneEtFormate1Garde :: Double -> String
borneEtFormate1Garde x = show x ++ " -> " ++ show x'
    where x' | x < 0 = 0
    | x > 1 = 1
    | otherwise = x