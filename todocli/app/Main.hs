import Text.Read
import System.Environment
import System.IO
import Data.List.Split

type Task = (Int, Bool, String)
type Todo = (Int, [Task])

display :: Task -> IO()
display (currentId, state, text) = do 
    if(state) then do
        putStrLn $ "[X] " ++ (show currentId) ++ ". " ++ text
    else do putStrLn $ "[-] " ++ (show currentId) ++ ". " ++ text

displayDone :: Task -> IO()
displayDone (currentId, done, text) = do
    if(done) then do
        putStrLn $ "[X] " ++ (show currentId) ++ ". " ++ text
    else do putStr ""

displayTodo :: Task -> IO()
displayTodo (currentId, done, text) = do 
    if(done) then do
        putStr ""
    else do putStrLn $ "[-] " ++ (show currentId) ++ ". " ++ text

todoPrint :: Todo -> String -> IO()
todoPrint todo param = do 
    case param of
        " " ->  mapM_ display (snd todo)
        "todo" -> mapM_ displayTodo (snd todo)
        "done" -> mapM_ displayDone (snd todo)
        _ -> putStrLn "usage: \n\
            \ print \n\
            \ print todo \n\
            \ print done"

doTask :: Task -> Int -> Task
doTask (currentId, state, text) idToDo 
    | (currentId == idToDo) = (currentId, True, text)
    | otherwise = (currentId, state, text)

undoTask :: Task -> Int -> Task
undoTask (currentId, state, text) idToDo 
    | (currentId == idToDo) = (currentId, False, text)
    | otherwise = (currentId, state, text)

add :: String -> Int -> Task
add text index
    | otherwise = (index, False, text)

cmd :: Todo -> String -> Int -> IO() 
cmd todo input index = do 
    let explode = splitOn " " input
    let param = if (length explode) > 1 then (head $ tail explode) else " "
    case head(explode) of
        "print" -> todoPrint todo param
        "do" -> putStrLn "0" --doTask todo param
        "undo" -> putStrLn "0" --undoTask todo param
        "add" -> putStrLn "0" -- add todo index
        "del" -> putStrLn "0" 
        "exit" -> putStrLn "0" -- writeFile filename todo
        _ -> putStrLn "usage: \n\
            \ print \n\
            \ print todo \n\
            \ print done \n\
            \ add <string> \n\
            \ do <int> \n\
            \ undo <int> \n\
            \ del <int> \n\
            \ exit"

cli :: Todo -> String -> Int -> IO()  
cli todo input index = do
    if input == "exit" then do
        putStrLn "bye, saved"
    else do
        putStr "> "
        input_cmd <- getLine
        cmd todo input_cmd index
        cli todo input_cmd index

main :: IO ()
main = do
    -- contents <- readFile "tasks.txt"
    -- let todo1 = read $! contents :: Todo
    -- print todo1
    -- mapM_ print (snd todo1)
    args <- getArgs
    case args of
        [filename] -> do
            contents <- readFile filename
            let todo = read $! contents :: Todo
            cli todo "" 0
        _ -> putStrLn "usage: <file>"
