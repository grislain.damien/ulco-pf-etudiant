fuzzyLength :: [a] -> String
fuzzyLength [] = "Empty"
fuzzyLength [a] = "One"
fuzzyLength [a, b] = "Two"
fuzzyLength _ = "Many"