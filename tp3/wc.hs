import System.Environment (getArgs)

compterLignes::String -> Int
compterLignes str = length (lines str)

compterMots::String -> Int
compterMots str = length (words str)

compterChars::String -> Int
compterChars str = length str

compterCharsNonVides::String -> Int
compterCharsNonVides str = length (concat (words str))

main :: IO ()
main = do
    args <- getArgs
    case args of
        [filename] -> do
            content <- readFile filename
            print (compterLignes content)
            print (compterMots content)
            print (compterChars content)
            print (compterCharsNonVides content)
        _ -> putStrLn "usage: <file>"