import Control.Monad (unless)
import System.Random (randomRIO)

loop :: Int -> Int -> IO()
loop win_number try_count = do
    if try_count /= 0 then do
        putStrLn $ "Nombre d'essai restant: " ++ show try_count
        putStr "Entrez un nombre: "
        number <- getLine
        let int_number = (read number :: Int)
        unless (int_number == win_number) $ do
            if int_number > win_number
                then putStrLn "Trop grand !"
                else putStrLn "Trop petit !"
            loop win_number (try_count - 1)
        putStrLn "Vous avez gagné !"
    else putStrLn "Vous avez perdu !"
        
main :: IO()
main = do
    let tmp_nb = 10
    win_number <- randomRIO (0, 10 :: Int)
    putStrLn "Devinez un nombre entre 1 et 10"
    loop win_number tmp_nb