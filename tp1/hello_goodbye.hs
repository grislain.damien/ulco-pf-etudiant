import Control.Monad (unless)

loop :: IO()
loop = do
    putStr "Entrez un nom: "
    name <- getLine
    unless (name == "") $ do
        putStrLn $ "Hello " ++ name ++ " !"
        loop
    
main :: IO()
main = do
    loop
    putStrLn "Good bye !"
