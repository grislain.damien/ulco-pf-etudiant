import System.Random as R

main = do
    random_number <- R.randomRIO (0, 100 :: Int)
    putStr $ show random_number
