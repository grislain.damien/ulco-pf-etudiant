add_to_int :: Num a => a -> a -> a
add_to_int x y =  x + y

add_to_double :: Num a => a -> a -> a
add_to_double x y = x + y

main :: IO ()
main = do
    let u = 2
    let v = 3.0
    print (add_to_int 2 3)
    print (add_to_double 2.0 3.0)
    print (add_to_double (u*u) (v*v))

